# Recipe App API Proxy

Niginx proxy app for our recipe app API

## Usage
### Environment Variables
* `LISTEN_PORT` - Port to listen on (default:`8080`)
* `APP_Host` - Hostname of the app to forward the request to (default:`app`)
* `App_Port` - Port of the app to forward the request to (default:`9000`)